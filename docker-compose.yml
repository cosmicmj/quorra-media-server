version: '3'

networks:
  # web:
  #   external:
  #     name: reverseproxy_default
  bridge:
    driver: bridge
    
services:
  proxy:
    image: traefik
    container_name: proxy
    command: --web.address=":${ADMIN_PORT}" --docker.domain="${DOCKER_DOMAIN}" --docker.usebindportip=true --acme.email="${ACME_EMAIL}"
    networks:
      # - web
      - bridge
    ports:
      - "80:80" # for the HTTP -> HTTPS redirection
      - "443:443"
      - "${ADMIN_PORT}:${ADMIN_PORT}"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./traefik.toml:/etc/traefik/traefik.toml:ro
      - ./acme/:/etc/traefik/acme

  plex:
    image: plexinc/pms-docker:latest
    container_name: plex
    restart: always
    ports:
      - "32400:32400"
    networks:
      # - web
      - bridge
    environment:
      - PLEX_CLAIM="${PLEX_TOKEN}"
      - ADVERTISE_IP="https://plex.${DOMAIN_NAME}/"
      - PLEX_UID=${USER_ID}
      - PLEX_GID=${GROUP_ID}
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./media/radarr/movies:/data/movies
      - ./media/sonarr/series:/data/tvshows
      - ./media/plex/config/:/config
      - /tmp/transcode/:/transcode
    labels:
      - "traefik.enable=true"
      - "traefik.backend=plex"
      - "traefik.frontend.rule=Host:plex.${DOMAIN_NAME}"
      - "traefik.port=32400"
      - "traefik.docker.network=web"
    depends_on:
      - proxy
      - transmission
      - sonarr
      - radarr

  plexpy:
    image: linuxserver/plexpy:latest
    container_name: plexpy
    restart: always
    ports:
      - "8181:8181"
    networks:
      # - web
      - bridge
    environment:
      - PUID=${USER_ID}
      - PGID=${GROUP_ID}
    volumes:
      - ./media/plexpy:/config
      - ./media/plex/config/Library/Application\ Support/Plex\ Media\ Server/Logs:/logs:ro
    labels:
      - "traefik.enable=true"
      - "traefik.backend=plexpy"
      - "traefik.frontend.rule=Host:plexpy.${DOMAIN_NAME}"
      - "traefik.port=8181"
      - "traefik.docker.network=web"
    depends_on:
      - proxy
      - plex

  transmission:
    image: haugene/transmission-openvpn:latest
    container_name: transmission
    restart: always
    privileged: true
    ports:
      - "9091:9091"
    networks:
      # - web
      - bridge
    environment:
      - PUID=${USER_ID}
      - PGID=${GROUP_ID}
      - OPENVPN_PROVIDER=${VPN_PROVIDER}
      - OPENVPN_CONFIG=${VPN_CONFIG}
      - OPENVPN_USERNAME=${VPN_USERNAME}
      - OPENVPN_PASSWORD=${VPN_PASSWORD}
      - OPENVPN_OPTS=--inactive 3600 --ping 10 --ping-exit 60
    devices:
      - /dev/net/tun
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./media/transmission/config/:/config
      - ./media/transmission/downloads/:/downloads
    labels:
      - "traefik.enable=true"
      - "traefik.backend=transmission"
      - "traefik.frontend.rule=Host:transmission.${DOMAIN_NAME}"
      - "traefik.port=9091"
      - "traefik.docker.network=bridge"
    depends_on:
      - proxy

  # transmission:
  #   image: linuxserver/transmission:latest
  #   container_name: transmission
  #   restart: always
  #   ports:
  #     - "9091:9091"
  #   networks:
  #     - web
  #   environment:
  #     - PUID=${USER_ID}
  #     - PGID=${GROUP_ID}
  #   volumes:
  #     - /etc/localtime:/etc/localtime:ro
  #     - ./media/transmission/config/:/config
  #     - ./media/transmission/downloads/:/downloads
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.backend=transmission"
  #     - "traefik.frontend.rule=Host:transmission.${DOMAIN_NAME}"
  #     - "traefik.port=9091"
  #     - "traefik.docker.network=web"

  sonarr:
    image: linuxserver/sonarr:latest
    container_name: sonarr
    restart: always
    ports:
      - "8989:8989"
    networks:
      # - web
      - bridge
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /dev/rtc:/dev/rtc:ro
      - ./media/sonarr/series:/tv
      - ./media/sonarr/config:/config
      - ./media/transmission/downloads/:/downloads
    environment:
      - PUID=${USER_ID}
      - PGID=${GROUP_ID}
    labels:
      - "traefik.enable=true"
      - "traefik.backend=sonarr"
      - "traefik.frontend.rule=Host:sonarr.${DOMAIN_NAME}"
      - "traefik.port=8989"
      - "traefik.docker.network=web"
    depends_on:
      - proxy
      - transmission

  radarr:
    image: linuxserver/radarr:latest
    container_name: radarr
    restart: always
    ports:
      - "7878:7878"
    networks:
      # - web
      - bridge
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /dev/rtc:/dev/rtc:ro
      - ./media/radarr/movies:/movies
      - ./media/radarr/config:/config
      - ./media/transmission/downloads/:/downloads
    environment:
      - PUID=${USER_ID}
      - PGID=${GROUP_ID}
    labels:
      - "traefik.enable=true"
      - "traefik.backend=radarr"
      - "traefik.frontend.rule=Host:radarr.${DOMAIN_NAME}"
      - "traefik.port=7878"
      - "traefik.docker.network=web"
    depends_on:
      - proxy
      - transmission

  # Proxy to a bunch of public trackers
  jackett:
    image: linuxserver/jackett:latest
    container_name: jackett
    restart: always
    ports:
      - "9117:9117"
    networks:
      # - web
      - bridge
    volumes:
      - ./media/jackett/config/:/config
      - ./media/transmission/downloads/:/downloads
    environment:
      - PUID=${USER_ID}
      - PGID=${GROUP_ID}
    labels:
      - "traefik.enable=true"
      - "traefik.backend=jackett"
      - "traefik.frontend.rule=Host:jackett.${DOMAIN_NAME}"
      - "traefik.port=9117"
      - "traefik.docker.network=web"
    depends_on:
      - proxy

  ombi:
    image: linuxserver/ombi
    container_name: ombi
    restart: always
    ports:
      - "3579:3579"
    networks:
      # - web
      - bridge
    environment:
      - PUID=${USER_ID}
      - PGID=${GROUP_ID}
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./media/ombi/config:/config
    labels:
      - "traefik.enable=true"
      - "traefik.backend=ombi"
      - "traefik.frontend.rule=Host:ombi.${DOMAIN_NAME}"
      - "traefik.port=3579"
      - "traefik.docker.network=web"
    depends_on:
      - proxy
      - sonarr
      - radarr
